
<?php get_header();?>

<?php 
/*
 Template Name: home
 */
?>

<?php include_once ('slider.php');?>

<!--------------Content--------------->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block02">
			<div class="col-2-3">
				<div class="wrap-col">
					<div class="heading"><h2>Latest Blog</h2></div>
                                        <?php $simple_post = new WP_Query(array(
                                            'post_type'=>'page',
                                            'posts_per_page'=>'5',
                                        ));?>
                                        
                                            <?php while ($simple_post->have_posts()) : $simple_post->the_post(); ?>
                                                <article class="row">
                                                        <div class="col-1-3">
                                                                <div class="wrap-col">
                                                                        <?php the_post_thumbnail(); ?>
                                                                </div>
                                                        </div>
                                                        <div class="col-2-3">
                                                                <div class="wrap-col">
                                                                    <h2><a href="#"><?php the_title(); ?></a></h2>
                                                                    <div class="info">By <?php the_author();?> <?php the_time('y d f g i a')?> with <a href="#"><?php comments_popup_link('Comment Nai','1 ta comment asa','','',''); ?></a></div>
                                                                    <p><?php read_more('7'); ?> <a href="<?php the_permalink();?>">Read More...</a></p>
                                                                   
                                                                </div>
                                                        </div>
                                                </article>
                                            <?php endwhile;?>
				</div>
                           
			</div>
                    <!--------------Start sidebar--------------->
			<?php get_sidebar();?>
                    <!--------------End Sidebar--------------->
		</div>
	</div>
</section>
<!--------------Footer--------------->
<?php get_footer();?>