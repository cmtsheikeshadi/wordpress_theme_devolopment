<?php get_header();?>

<!--------------Content--------------->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block02">
			<div class="col-2-3">
				<div class="wrap-col">
					<div class="heading"><h2>Latest Blog</h2></div>
                                        
                                            <?php while (have_posts()) : the_post(); ?>
                                                <article class="row">
                                                        <div class="col-1-3">
                                                                <div class="wrap-col">
                                                                        <?php the_post_thumbnail(); ?>
                                                                </div>
                                                        </div>
                                                        <div class="col-2-3">
                                                                <div class="wrap-col">
                                                                    <h2><a href="#"><?php the_title(); ?></a></h2>
                                                                    <div class="info">By <?php the_author();?> <?php the_time('y d f g i a')?> with <a href="#"><?php comments_popup_link('Comment Nai','1 ta comment asa','','',''); ?></a></div>
                                                                    <p><?php the_content(); ?></p>
                                                                   
                                                                         <?php comments_template(); ?>
                                                                    
                                                                </div>
                                                        </div>
                                                </article>
                                            <?php endwhile;?>
				</div>
                           
                            
			</div>
        <!--------------Start sidebar--------------->
            <?php get_sidebar();?>
        <!--------------End Sidebar--------------->
                </div>
	</div>
</section>

<!--------------Footer--------------->
<?php get_footer(); ?>