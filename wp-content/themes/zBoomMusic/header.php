<head>

    <!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
        <meta name="description" content="Free Html5 Templates and Free Responsive Themes Designed by Kimmy | zerotheme.com">
	<meta name="author" content="www.zerotheme.com">
	
    <!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href='<?php echo get_template_directory_uri()?>/images/favicon.ico' rel='icon' type='image/x-icon'/>
	
        
<?php wp_head(); ?>        
</head>
<body>
<!--------------Header--------------->
<header>
	<div class="wrap-header zerogrid">
            <div id="logo">
                <a href="#">
                    <img src="<?php
                        global $redux_demo;
                        echo $redux_demo['header-option']['url'];
                    ?>">
                </a>
            </div>
		
		<div id="search">
                    <form action="<?php esc_url(bloginfo('home')); ?>" method="GET">
			<div class="button-search"></div>
                        <input type="text" value="" name="s" placeholder="Search">
                   </form>
                </div>
	</div>
</header>

<nav>
	<div class="wrap-nav zerogrid">
		<div class="menu">
			<?php wp_nav_menu(array(
                            'theme_location'=>'headmenu'
                        ));?>
		</div>
		
		<div class="minimenu"><div>MENU</div>
			<select onchange="location=this.value">
				<option></option>
				<option value="index.html">Home</option>
				<option value="blog.html">Blog</option>
				<option value="gallery.html">Gallery</option>
				<option value="single.html">About</option>
				<option value="contact.html">Contact</option>
			</select>
		</div>		
	</div>
</nav>

