
<?php get_header();?>

<?php 
/*
 Template Name: service
 */
?>

<!--------------Content--------------->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block02">
			<div class="col-2-3">
				<div class="wrap-col">
					<div class="heading">  </div>
                                       
                                        
                                            <?php while (have_posts()) : the_post(); ?>
                                                <article class="row">
                                                    <div class="wrap-col">
                                                        <h2><a href="#"><?php the_title(); ?></a></h2>
                                                        <div class="info"><?php the_content();?></div>

                                                    </div>
                                                </article>
                                            <?php endwhile;?>
				</div>
                           
			</div>
                    <div class="col-1-3">
                    <!--------------Start sidebar--------------->
			<?php dynamic_sidebar('contact_sidebar');?>
                    <!--------------End Sidebar--------------->
                    </div>
		</div>
	</div>
</section>
<!--------------Footer--------------->
<?php get_footer();?>