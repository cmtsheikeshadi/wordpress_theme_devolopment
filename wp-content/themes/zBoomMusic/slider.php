
<div class="featured">
	<div class="wrap-featured zerogrid">
		<div class="slider">
			<div class="rslides_container">
                            
                            <?php $slider_image= new WP_Query(array(
                                'post_type'=>'slider',
                                'posts_per_page'=>'3',
                            ));?>
				<ul class="rslides" id="slider">
                                    <?php  while ($slider_image->have_posts()): $slider_image->the_post()?>
                                    <li><?php the_post_thumbnail(); ?></li>
                                    <?php endwhile;?>
                                </ul>
			</div>
		</div>
	</div>
</div>