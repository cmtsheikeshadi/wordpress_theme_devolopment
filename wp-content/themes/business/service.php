<?php get_header();?>
<?php 
/*
 Template Name: Service
 */
?>

<div id="tooplate_main">
    
 <?php while (have_posts()) : the_post(); ?>
    <div id="tooplate_content">
        <h2><?php the_title(); ?></h2>
        <div class="image_wrapper fl_img"><?php the_post_thumbnail(); ?></div>
        <p> <?php the_content(); ?> </p>
        <div class="button float_r"><a href="#">More...</a></div>
        <div class="cleaner_h30 horizon_divider"></div>
    </div>
 <?php endwhile;?>
    
    <?php get_sidebar(); ?>
    <div class="cleaner"></div>
</div>

<?php get_footer(); ?>