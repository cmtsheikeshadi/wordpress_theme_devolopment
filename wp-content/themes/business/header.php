<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="<?php echo get_template_directory_uri();?>/tooplate_style.css" rel="stylesheet" type="text/css" />

<?php wp_head();?>
</head>
<body <?php body_class();?>> 

<div id="tooplate_header_wrapper">

    <div id="tooplate_header">
    
        <div id="site_title">
        
            <h1><a href="#"><img src="<?php header_image();?>" alt="business template" /><span><?php bloginfo('title');?></span></a></h1>
        
        </div> <!-- end of site_title -->
        
        <div id="header_phone_no">

			Toll Free: <span>08 324 552 409</span>
        
        </div>
        
        <div class="cleaner_h10"></div>
        
        <div id="tooplate_menu">
        	
            <div id="home_menu"><a href="#"></a></div>
                
            <?php wp_nav_menu(array(
                'theme_location'=>'headme'
            ));?>   	
        
        </div> <!-- end of tooplate_menu -->
        
    </div>	  
</div> <!-- end of header_wrapper -->
