<?php

add_theme_support('title-tag');
add_theme_support('custom-background');
add_theme_support('post-thumbnails');
add_theme_support('custom-header', array(
        'default-image' => get_template_directory_uri().'/images/tooplate_logo.png'
        ));

register_nav_menus(array(
            'headmenu'=>'Head Menu',
            'footermenu'=>'Footer Menu'
));


function left_sidebar(){
    register_sidebar(array(
        'name'=>'Left 1',
        'description'=>'Just catagory Add',
        'id'=>'left1',
        'before_title'=>'<h2>',
        'after_title'=>'</h2>',
        'before_widget'=>'<ul class="tooplate_list">',
        'after_widget'=>'</ul>',
    ));
    
    register_sidebar(array(
         'name'=>'Footer',
         'description'=>'Just footer copy right',
         'id'=>'footer',
         'before_title'=>'',
         'after_title'=>'',
         'before_widget'=>'',
         'after_widget'=>'',
     ));

}

add_action('widgets_init','left_sidebar');


?>